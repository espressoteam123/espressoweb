import Vue from 'vue'
import VueRouter from 'vue-router'
import VueApollo from 'vue-apollo'

import { HttpLink } from 'apollo-link-http'
import { ApolloLink } from 'apollo-link'
import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'

import BootstrapVue from "bootstrap-vue"
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"

import Routes from './router.js'
import App from './App.vue'


Vue.use(BootstrapVue)
Vue.use(VueRouter)

/* Connecting to Apollo Client, which connects to Realm Platform */
/* ************************************************************* */
const httpLink = new HttpLink({
    // Realm Platform link
    uri: 'https://espresso.us1.cloud.realm.io/graphql/default',
    credentials: 'same-origin'
})

const authLink = new ApolloLink((operation, forward) => {
    // Authorization
    const token = 'eyJhcHBfaWQiOiJpby5yZWFsbS5BdXRoIiwiaWRlbnRpdHkiOiIwN2QwYTE5NDIyN2ZkZjAwNDhjMTY2ZDgyOGNjOWYwYSIsImFjY2VzcyI6WyJyZWZyZXNoIl0sInNhbHQiOiJhYWRhYTQ4MyIsImV4cGlyZXMiOjE4NTMxNTM2MjQsImlzX2FkbWluIjp0cnVlLCJpc0VtYWlsQ29uZmlybWVkIjpmYWxzZX0=:fqo9FJnjahMCP2LX28BXRrYfgKWKF8JCsVi3nvGzwXSNfvULtzHn3Zi5H8Slzee0UHSzMbUik2uQoL+a9kSEeC+1don9RtmJ6CjQ1tyaoPgi9H3NM1OQFbIgiGAJM2NNP/b+aw7B66pQb0xhCIUWyXrZ7D797RuJX8YPtqAw0hjX7YrY07BcK9Ip0sJ+1PXLkNQzljiAMVGDZwA9LDqMBZqWdJ+qKJvZkdhtKUaUkCSMAWx19TjabdUUagAwVmPtFbMQwAoudEUOKJr8EjrDfqVUZqkTlgakZQ2wEN/qanfKsMyNXFBwKi73QbrB4HEKl3HGTp9jm0L4okO4btstyg=='
    operation.setContext({
        headers: {
            authorization: token
        }
    })
    return forward(operation)
})

const apolloClient = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache(),
    connectToDevTools: true
})

Vue.use(VueApollo)

const apolloProvider = new VueApollo({
    defaultClient: apolloClient,
    defaultOptions: {
        $loadingKey: 'loading'
    }
})



import LandingPage from './components/LandingPage.vue';
import Home from './components/Home.vue';
import Register from './components/Register.vue';
import Account from './components/Account.vue'



const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            redirect: {
                name: "login"
            }
        },
        {
            path: "/login",
            name: "login",
            component: LandingPage
        },
        {
            path: "/register",
            name: "register",
            component: Register
        },
        {
            path: "/home",
            name: "home",
            component: Home
        },
        {
            path: "/account",
            name: "account",
            component: Account
        },
        {
            path: "*",
            redirect: "/home"
        }
    ]
});

new Vue({
    //router: Routes,
    el: '#app',
    provide: apolloProvider.provide(),
    //apolloProvider,
    //apolloClient,
    render: h => h(App),
    router: router
})
