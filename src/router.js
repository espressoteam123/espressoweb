import Vue from 'vue';
import Router from 'vue-router';

import LandingPage from './components/LandingPage.vue';
import Home from './components/Home.vue';
import Register from './components/Register.vue';

Vue.use(Router);

export default new Router ({
    routes: [
        {
            path: '/',
            redirect: {
                name: "login"
            }
        },
        {
            path: "/login",
            name: "login",
            component: LandingPage
        },
        {
            path: "/register",
            name: "register",
            component: Register
        },
        {
            path: "/home",
            name: "home",
            component: Home
        },
        {
            path: "*",
            redirect: "/home"
        }
    ]
})
