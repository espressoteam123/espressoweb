import gql from 'graphql-tag'

/* Queries users and filters by username given in username_query */

export const SEARCH_USER_BY_USERNAME_QUERY = gql `
    query SearchUserByUsername ($filter: String!) {
        searchUsername: users (query: $filter) {
            username,
            name,
            password,
            userType
        }
    }
`

export const SEARCH_STORE = gql`
    query SearchStore ($filter: String!) {
        searchStore: baristas (query: $filter) {
            storeName,
            menu {
            	menuList {
            		itemName,
            		price,
            		description
            	}
            },
            user {
            	username,
            	name,
            	password,
            	userType
            }
        }
    }
`

export const SEARCH_ORDER = gql`
	query SearchOrder ($filter: String!) {
		searchOrder: customerOrderss (query: $filter){
			orderId,
			menuOrder,
			customerUsername,
			baristaUsername,
			orderStatus,
			time
		}
	}
`



export const ADD_BARISTA_MUTATION = gql`
	mutation AddBarista (

		$username: String!,
		$shopname: String!,
		$email: String!,
		$password: String!,
		$menu: [MenuItemInput],

	) {
		addBarista (
			input: {
				user: {
					username: $username,
					name: $shopname,
					email: $email,
					dob: "1/1/1111",
					userType:"barista",
					password: $password
				},
				menu: {
					menuList: $menu,
				},
				storeName: $shopname
			}
		) {
			user {
				username,
				name,
				password
			},
			menu {
				menuList {
					itemName,
					description,
					price
				}
			}
		}
	}
`